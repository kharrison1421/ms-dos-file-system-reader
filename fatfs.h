/*
 * fatfs.h
 *
 * Definition of the structure used to represent a FAT filesystem.
 */

#ifndef FATFS_H
#define FATFS_H

#include <inttypes.h>

typedef struct filesystem_info{
	uint16_t sector_size;
	uint16_t cluster_size;
	uint16_t root_entries;
	uint16_t sectors_per_fat;
	uint16_t reserved_sectors;
	uint16_t hidden_sectors;
	uint16_t first_fat_copy;
	uint16_t first_root_sector;
	uint16_t first_usable_cluster;
} *filesystem_info;

#endif
