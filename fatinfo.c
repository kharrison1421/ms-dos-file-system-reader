/*
 * Program to print information about a FAT file system.
 */

#include <stdio.h>
#include <inttypes.h>
#include "fatfs.h"
#include "directory.h"

int main(int argc, char *argv[]){
	FILE *input;
	char *filename, *buf;
	int sector_size = 512;
	filesystem_info fs_info;
	directory_info dir_info;
	
	if(argc == 1)
		filename = "/dev/stdin";
	else
		filename = argv[1];
		
	input = fopen(filename, "r");
	if(!input){
		perror("Could not open file");
		return 2;
	}
	else{
		printf("%s open! \n\n", filename);
		
		//Get filesystem info
		fs_info = initialize_filesystem();
		buf = read_sector(&input, sector_size); //Read boot sector
		get_filesystem_info(&fs_info, buf); //Extract info from boot sector
		print_filesystem(&fs_info);
		
		//Traverse directories, starting at root
		dir_info = initialize_directory();
		get_directory_info(&input, &fs_info, &dir_info, "/"); 

		fclose(input);
	}
}
