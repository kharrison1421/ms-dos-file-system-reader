/*
 * directory.h
 *
 * Functions used to deal with directory entries.
 */

#ifndef DIRECTORY_H
#define DIRECTORY_H

#include <inttypes.h>

typedef struct directory_info{
	char *file_name;
	char *file_extension;
	uint16_t is_directory;
	uint16_t first_cluster;
	uint16_t file_size;
} *directory_info;

#endif
