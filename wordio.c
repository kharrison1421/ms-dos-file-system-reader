/*
 * wordio.c
 *
 * Low-level functions used to perform  operations on words (normally 1, 2 or
 * 4 bytes) contained in a buffer.
 */
#include <inttypes.h>
#include "wordio.h"

uint64_t extract_value(char *buf, int start, int end){
	int number_of_bytes = end - start + 1;
	uint64_t value;
	
	if(number_of_bytes == 1)
		value = buf[start] & 0x0FF;
	else if(number_of_bytes == 2)
		value = (buf[end] << 8) | (buf[start] & 0x0FF);
	else if(number_of_bytes == 3)
		value = (buf[end] << 16) | (buf[end-1] << 8) | (buf[start] & 0x0FF);
	else if(number_of_bytes == 4)
		value = (buf[end] << 24) | (buf[end-1] << 16) | (buf[end-2] << 8) | (buf[start] & 0x0FF);

	return value;
}

char *extract_string(char *buf, int start, int end){
	int i, number_of_bytes = end - start + 1;
	char *text;
	
	text = malloc(number_of_bytes * sizeof(char));
	for(i=0; i<number_of_bytes; i++)
		text[i] = buf[start + i];

	return text;
}
