/*
 * blockio.c
 *
 * Low-level functions used to perform  i/o operations on blocks. All of them
 * take as parameter an object  that contains information about the file that
 * is being accessed, the size of a sector, etc.
 */

#include <stdio.h>
#include <inttypes.h>
#include "blockio.h"
#include "fatfs.h"

char *read_sector(FILE *input, int sector_size){
	char *buf;
	buf = (char *)malloc(sector_size);
	int chars_read = read(3, buf, sector_size);
	if(chars_read < 0)
		perror("Error reading:");

	return buf;
}

char *read_cluster(FILE *input, filesystem_info *fs_info, uint16_t sector){
	uint16_t sector_size = (*fs_info)->sector_size;
	uint16_t cluster_size = (*fs_info)->cluster_size;
	uint16_t cluster_bytes = sector_size * cluster_size;
	uint16_t offset = sector_size * sector;
	
	lseek(3, offset, SEEK_SET);
	
	char *buf;
	buf = (char *)malloc(cluster_bytes);
	int chars_read = read(3, buf, cluster_bytes);
	if(chars_read < 0)
		perror("Error reading:");
		
	return buf;
}

char *read_fat(FILE *input, filesystem_info *fs_info, uint16_t entry){
	uint16_t offset = entry * 1.5;
	uint16_t fat_entry;
	char buf[3];
	
	//I know this is wrong.
	if(offset % 3 == 0){
		offset = offset + 511;
		lseek(3, offset, SEEK_SET);
		read(3, buf, 3);
		fat_entry = extract_value(&buf, 0, 2);
		fat_entry = (fat_entry >> 12) & 0x0FFF;
	}
	else if(offset % 3 == 1){
		offset = offset + 3 + 511;
		lseek(3, offset, SEEK_SET);
		read(3, buf, 3);
		fat_entry = extract_value(&buf, 0, 2);
		fat_entry = (fat_entry >> 12) & 0x0FFF;
	}
	else if(offset % 3 == 2){
		offset = offset + 2 + 511;
		lseek(3, offset, SEEK_SET);
		read(3, buf, 3);
		fat_entry = extract_value(&buf, 0, 2);
		fat_entry = (fat_entry >> 12) & 0x0FFF;
	}
	printf("%x \n", fat_entry);

	return buf;
}
