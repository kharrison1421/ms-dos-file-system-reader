/*
 * directory.c
 *
 * Functions used to deal with directory entries.
 */

#include <stdio.h>
#include "directory.h"
#include "fatfs.h"

directory_info initialize_directory(void){
	return (directory_info)malloc(sizeof(struct directory_info));
}

void get_directory_info(FILE *input, filesystem_info *fs_info, directory_info *dir_info, char *path){
	uint16_t sector; 
	uint16_t data;
	char *buf;	
	char *text;
	char filepath[1000];
	char wholepath[1000];
	int start = 0;
	int end = 7;

	if(path == "/"){
		sector = (*fs_info)->first_root_sector;
		strcpy(filepath, path);
	}
	else{
		strcat(filepath, (*dir_info)->file_name);
	}
	
	buf = read_cluster(input, fs_info, sector);
	
	do{ //Read each 32 byte root directory entry. Offsets were calculated so as to be able to use same buffer
		
		//Extract file name
		text = extract_string(buf, start, end);
		(*dir_info)->file_name = text;
		
		//Extract file extension
		start += 8;
		end += 3;
		text = extract_string(buf, start, end);
		(*dir_info)->file_extension = text;
		
		//Extract location first cluster that data is stored
		start += 18;
		end += 17;
		data = extract_value(buf, start, end);
		(*dir_info)->first_cluster = data;
		
		//Extract filesize
		start += 2;
		end += 4;
		data = extract_value(buf, start, end);
		(*dir_info)->file_size = data;
		
		//If filesize is 0, is a directory
		if(data == 0x00){
			(*dir_info)->is_directory = 1; //Is a directory
			strcat(filepath, (*dir_info)->file_name); //Add to filepath
			realpath((*dir_info)->file_name, wholepath);
		}
		else{
			(*dir_info)->is_directory = 0; //Not a directory
			strcat(filepath, (*dir_info)->file_name); //Add to filepath
			strcat(filepath, ".");
			strcat(filepath, (*dir_info)->file_extension);
			realpath((*dir_info)->file_name, wholepath);
			strcat(wholepath, ".");
			strcat(wholepath, (*dir_info)->file_extension);
		}

		if( ((*dir_info)->file_name[0] & 0x0FF) != 0xe5 ){ //Don't print deleted entries
			uint16_t sector = get_sector_from_fat(input, fs_info, (*dir_info)->first_cluster);

			printf("\nFilepath: %s", wholepath);
			print_directory(dir_info);
		}
		
		//Get next entry
		start += 4;
		end += 8;
		
		strcpy(filepath, path); //Begin new file path beginning at root
	}while((*dir_info)->file_name[0] != 0x00); //End of directory entries
}

void print_directory(directory_info *dir_info){
	printf("\nName: %s.%s \n", (*dir_info)->file_name, (*dir_info)->file_extension);
	printf("Is it a directory: %d \n", (*dir_info)->is_directory);
	printf("Number of first cluster containing data: %d \n", (*dir_info)->first_cluster);
	printf("File size: %d \n", (*dir_info)->file_size);
}
