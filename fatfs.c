/*
 * fatfs.c
 *
 * Basic operations on a FAT filesystem.
 */

#include <inttypes.h>
#include "fatfs.h"

filesystem_info initialize_filesystem(void){
	return (filesystem_info)malloc(sizeof(struct filesystem_info));
}

void get_filesystem_info(filesystem_info *fs_info, char *buf){
	int start, end;
	uint64_t data;
	
	//Extract sector size
	start = 11;
	end = 12;
	data = extract_value(buf, start, end); 
	(*fs_info)->sector_size = data;
	
	//Extract cluster size
	start = 13;
	end = 13;
	data = extract_value(buf, start, end); 
	(*fs_info)->cluster_size = data;
	
	//Extract number of entries in root directory
	start = 17;
	end = 18;
	data = extract_value(buf, start, end); 
	(*fs_info)->root_entries = data;
	
	//Extract number of sectors per FAT
	start = 22;
	end = 23;
	data = extract_value(buf, start, end); 
	(*fs_info)->sectors_per_fat = data;
	
	//Extract number of reserved sectors
	start = 14;
	end = 15;
	data = extract_value(buf, start, end); 
	(*fs_info)->reserved_sectors = data;
	
	//Extract number of hidden sectors
	start = 28;
	end = 29;
	data = extract_value(buf, start, end); 
	(*fs_info)->hidden_sectors = data;

	//Calculate sector number first copy of FAT
	(*fs_info)->first_fat_copy = 1;
	
	//Calculate sector number of first sector of root directory
	start = 16;
	end = 16;
	data = extract_value(buf, start, end); //Number of FATs 
	data = data * (*fs_info)->sectors_per_fat + 1;
	(*fs_info)->first_root_sector = data;
	
	//Calculate size of root directory
	uint16_t root_size = ((*fs_info)->root_entries * 32) % (*fs_info)->sector_size;
	if(root_size != 0) //Round to the next sector
		root_size = (int)( ((*fs_info)->root_entries * 32) / (*fs_info)->sector_size ) + 1;
	else
		root_size = ((*fs_info)->root_entries * 32) / (*fs_info)->sector_size;
		
	//Calculate sector number of first sector of first usable data cluster
	uint16_t begin_data_area = (*fs_info)->first_root_sector + root_size;
	uint16_t offset_clusters = (*fs_info)->cluster_size * 2; //First usable data is always in 2nd cluster of data
	(*fs_info)->first_usable_cluster = begin_data_area + offset_clusters;
}

void print_filesystem(filesystem_info *fs_info){
	printf("Sector size: %d bytes \n", (*fs_info)->sector_size);
	printf("Cluster size: %d sectors\n", (*fs_info)->cluster_size);
	printf("Number of root entries: %d entries \n", (*fs_info)->root_entries);
	printf("FAT size: %d sectors \n", (*fs_info)->sectors_per_fat);
	printf("Reserved sectors: %d sector(s) \n", (*fs_info)->reserved_sectors);
	printf("Hidden sectors: %d sector(s) \n", (*fs_info)->hidden_sectors);
	printf("First sector of first FAT copy: sector %d \n", (*fs_info)->first_fat_copy);
	printf("First sector of root directory: sector %d \n", (*fs_info)->first_root_sector);
	printf("First sector of usable data cluster: sector %d \n", (*fs_info)->first_usable_cluster);
}
